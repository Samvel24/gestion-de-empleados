# Gestión de empleados por medio de Spring Framework
Proyecto para la gestión de empleados utilizando `Spring Framework (Core)`. Este proyecto está basado en un ejemplo proporcionado en el [Curso de Spring framework de Píldoras Informáticas](https://www.pildorasinformaticas.es/course/curso-spring/)

### Configuración
* Requerimentos
	1. OpenJDK versión 11
    2. Spring Tool Suite 4
    3. Maven

* Dependencias utilizadas para la construcción y funcionamiento de este proyecto  
    1. [Spring Core](https://mvnrepository.com/artifact/org.springframework/spring-core)
    2. [Spring Context Support](https://mvnrepository.com/artifact/org.springframework/spring-context-support)

***
2023 [Samuel Ramirez](https://gitlab.com/Samvel24)