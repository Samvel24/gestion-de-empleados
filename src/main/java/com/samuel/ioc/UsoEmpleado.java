package com.samuel.ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoEmpleado {

	public static void main(String[] args) {
		/*
		Existe un problema con este enfoque de aplicación, si necesitamos un nuevo objeto, 
		tenemos que hacer cambios de código en la aplicación, o bien, si ya no necesitamos 
		cierto objeto, tendríamos que eliminar la clase y todas las líneas de código que 
		hicieron uso de ese objeto, en esta aplicación pequeña no es un problema porque
		los cambios requeridos se pueden ejecutar facilmente, sin embargo en aplicaciones 
		de gran tamaño esto se puede volver dificil de solucionar, para resolver esto, entra 
		en juego Spring con la Inversión de control, con sus Beans y con su ObjectFactory.
		*
		/*
		// Creación de objetos de tipo empleado
		Empleado empleado1 = new DirectorEmpleado();
		
		// Uso de los objetos creados
		System.out.println(empleado1.getTareas());
		*/
		
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("com/samuel/ioc/applicationContext.xml");
		/*Empleado Samuel = contexto.getBean("miEmpleado", Empleado.class);
		System.out.println(Samuel.getTareas());*/
		/* 
		 Este código nos permite ver que no se ha usado la palabra clave new para crear 
		 un objeto de tipo Informe con el objetivo de llamar a su método getInforme(), 
		 Spring ya se ha encargado de esta tarea.
		 */
		/*System.out.println(Samuel.getInforme());*/
		
		/*
		SecretarioEmpleado Miriam = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
		System.out.println(Miriam.getTareas());
		System.out.println(Miriam.getInforme());
		
		System.out.println("Email: " + Miriam.getEmail());
		System.out.println(Miriam.getNombreEmpresa());
		*/
		
		DirectorEmpleado Victor = contexto.getBean("miEmpleado", DirectorEmpleado.class);
		System.out.println(Victor.getTareas());
		System.out.println(Victor.getInforme());
		
		System.out.println("Email: " + Victor.getEmail());
		System.out.println(Victor.getNombreEmpresa());
		
		contexto.close();
	}
}
