package com.samuel.ioc;

public class JefeEmpleado implements Empleado {
	// Campo tipo CreacionInformes (interface)
	private CreacionInformes informeNuevo;
		
	// Constructor que inyecta la dependencia
	public JefeEmpleado(CreacionInformes informeNuevo) {
		this.informeNuevo = informeNuevo;
	}
	
	public String getTareas() {
		return "Gestiono las cuestiones relativas a los trabajadores del área";
	}

	@Override
	public String getInforme() {
		// TODO Auto-generated method stub
		return "Informe creado por el Jefe de área: " + informeNuevo.getInforme();
	}
}
