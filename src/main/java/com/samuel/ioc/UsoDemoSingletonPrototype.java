package com.samuel.ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoDemoSingletonPrototype {

	public static void main(String[] args) {
		// Carga de XML de configuración
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("com/samuel/ioc/applicationContext2.xml");
		
		// Petición de beans al contendor de Spring
		SecretarioEmpleado Itzel = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
		SecretarioEmpleado Alberto = contexto.getBean("miSecretarioEmpleado", SecretarioEmpleado.class);
		
		/*
		Cuando se llama a System.out.println, internamente se llama al método toString 
		que devuelve una cadena con el código hash del objeto, cuando dos objetos son 
		iguales entonces el código hash de los objetos debe ser el mismo.
		*/
		System.out.println(Itzel);
		System.out.println(Alberto);
		contexto.close();
	}
}
