package com.samuel.ioc;

public class DirectorEmpleado implements Empleado {
	// Campo tipo CreacionInformes (interface)
	private CreacionInformes informeNuevo;
	private String email;
	private String nombreEmpresa;
	
	// Constructor que inyecta la dependencia
	public DirectorEmpleado(CreacionInformes informeNuevo) {
		this.informeNuevo = informeNuevo;
	}
	
	@Override
	public String getTareas() {
		return "Gestionar la plantilla de la empresa";
	}

	@Override
	public String getInforme() {
		/*
		En la siguiente instrucción se llama al método getInforme() del objeto de la 
		clase que se pasa en el constructor, en este caso es un objeto de tipo Informe 
		*/
		return "Informe creado por el director: " + informeNuevo.getInforme();
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombreEmpresa() {
		return nombreEmpresa;
	}

	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}
	
	/*
	El método init ejecuta tareas antes de que el Bean esté disponible
	
	No es necesario llamar init() a este método que ejecuta las tareas antes de la 
	creación de un Bean, se puede llamar de la manera que consideremos necesaria
	*/
	public void metodoInicial() { 
		System.out.println("Dentro del método init, aquí van las tareas a ejecutar"
							+ " antes de que el Bean esté listo.");
	}
	
	/*
	El método destroy ejecuta tareas después de que el Bean haya sido utilizado 
	*/
	public void metodoFinal() { 
		System.out.println("Dentro del método destroy, aquí van las tareas a ejecutar"
							+ " después de utilizar el Bean.");
	}
}
