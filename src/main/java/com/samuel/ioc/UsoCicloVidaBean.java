package com.samuel.ioc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UsoCicloVidaBean {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext contexto = new ClassPathXmlApplicationContext("com/samuel/ioc/applicationContext3.xml");
		Empleado Juan  = contexto.getBean("miEmpleado", Empleado.class);
		System.out.println(Juan.getInforme());
		contexto.close();
	}
}
